=== Habitat  ===
Requires at least: WordPress 4.7
Tested up to: WordPress 5.9.0
Stable tag: 1.1.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Theme for Habitat National Sites.

== Installation ==


== Changelog ==

= 1.2.0 =
* Released: January 25, 2023

- Add submenu expanding to mobile menu
- Add Linkedin social network

= 1.1.9 =
* Released: July 8, 2022

- Update Timber to 1.20.0
- Add PHP 8.0 compatibility

= 1.1.8 =
* Released: July 7, 2022

- Fix missing translations
- Fix Expandable teaser close button text
- Adjust Main Menu and Footer Brands links when empty
- Update node packages
- Update gulp script

= 1.1.7 =
* Released: June 15, 2022

- Fix fatal when activating ACF for the first time

= 1.1.6 =
* Released: June 4, 2022

- Add possibility to select Categories/Tags in Archive Teaser Grid component

= 1.1.5 =
* Released: January 27, 2022

- Fix Gravity forms 2.5 validation message markup
- Fix Twig whitespaces issue on PHP 7.4

= 1.1.4 =
* Released: August 12, 2021

- Fix Hero classes

= 1.1.3 =
* Released: September 30, 2019

- Allow pages in Theme Options to be set to null

= 1.1.2 =
* Released: May 29, 2019

- Fix fatal errors when ACF plugin is disabled


= 1.1.1 =
* Released: February 20, 2018

- Fix issue with footer menu


= 1.1.0 =
* Released: January 30, 2018

Warning: This is MAJOR update, so please backup your website before downloading it!

- Adjust Expendable teaser component
- Adjust Custom Teaser Grid component
- Adjust article meta (Hide and Show share fields)
- Adjust blockquote styles
- Adjust assets loading
- Add Polylang compatibility with ACF options
- Fix share buttons styles
- Fix list styles
- Fix Blog layout (3col + 1fullwidth)
- Fix Custom Teaser Grid missing excerpt
- Add target=“_blank” to global social buttons
- Remove Timmy package
- Update Twig to 1.35
- Update theme dependencies
- and many minor fixes and adjustments..


= 1.0.0 =
* Released: August 30, 2017

- Initial release

<?php
/**
 * The template for displaying downloads.
 *
 * Template Name: Events
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

// Prepare data for Archive Teaser Grid flexible component (if exists)
$args = habitat_archive_teaser_grid_query_args( $post );
if ( $args ) {
	$context['archive_teaser_posts'] = new Timber\PostQuery( $args );
}

// Prepare data for Sub Pages Grig flexible component (if exists)
$args = habitat_sub_pages_grid_query_args( $post );
$context['sub_pages'] = Timber::get_posts( $args );

$event_args = array(
	'post_type' => 'event',
	'posts_per_page' => 10,
	'facetwp' => true,
);
$context['posts'] = Timber::get_posts( $event_args );

$templates = array( 'template-events.twig', 'page.twig' );
Timber::render( $templates, $context );

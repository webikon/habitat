(function($) {
	/**
	 * The following code will generate a “Load more” button directly below your FacetWP template.
	 * Clicking the button will append new results to the listing (similar to Infinite Scroll).
	 *
	 * Source: https://facetwp.com/how-to-add-a-load-more-button/
	 */
	$(function() {
		if ('object' != typeof FWP) {
			return;
		}

		wp.hooks.addFilter('facetwp/template_html', function(resp, params) {
			if (FWP.is_load_more) {
				FWP.is_load_more = false;
				$('.facetwp-template').append(params.html);
				return true;
			}
			return resp;
		});
	});

	$(document).on('click', '.js-fwp-load-more', function() {
		$('.js-fwp-loader').show();
		$('.js-fwp-load-more').hide();
		FWP.is_load_more = true;
		FWP.paged = parseInt(FWP.settings.pager.page) + 1;
		FWP.soft_refresh = true;
		FWP.refresh();
	});

	$(document).on('facetwp-loaded', function() {
		if (FWP.settings.pager.page < FWP.settings.pager.total_pages) {
			$('.js-fwp-load-more').show();
		}
		else {
			$('.js-fwp-load-more').hide();
		}

		// Hide svg loader
		$('.js-fwp-loader').hide();
	});

	$(document).on('facetwp-refresh', function() {
		if (! FWP.loaded) {
			FWP.paged = 1;
		}
	});




	/**
	 * Reset current filter by clicking "All" radio input.
	 *
	 */
	$(document).on('click', '.js-fwp-reset', function() {
		// Get current filter name, so we can reset proper filter
		var $current_facet = $(this).parent().data('name');

		// Reset only one filter or fallback to reset all
		if( $current_facet.length > 0 ) {
			// This reset is taken from FWP.reset() function, but customized to reset only one filter.
			FWP.parse_facets();
			FWP.is_reset = true;
			FWP.facets[$current_facet] = [];
			FWP.used_facets = {};
			FWP.refresh();
		} else {
			FWP.reset();
		}
	});

})(jQuery);

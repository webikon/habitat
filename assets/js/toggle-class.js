/**
 * ToggleClass
 * To toggle a class on click or tap use the data-toggle-class helper.
 *
 * Usage: Add the following data attributes to your button or link.
 * data-toggle-class="is-visible" - The selector you'd like to be applied to the toggled element.
 * data-toggle-class="is-visible" - The selector you'd like to be applied to the toggled element.
 * data-target=".some-div" - The selector of the element you'd like toggled. This is where the class will be applied.
 *
 * Note: The button or link you are using will have the class is-active applied to it when the target element is toggled 'on'.
 *
 */

/* using forEach function */

(function() {
	(function(document, forEach) {
		'use strict';

		// Find all toggle class items and add them to an array.

		var toggleItems = document.querySelectorAll('[data-toggle-class]');

		// Make sure the toggleable items exist.
		if (toggleItems.length) {
			forEach(toggleItems, function(index, toggleItem) {

				// Get the target css selector for the target container.
				var toggleTargetSelector = toggleItem.getAttribute('data-target');

				// Get boolean if target selector is parent of current toggle item
				var isToggleParentContainer = toggleItem.getAttribute('data-parent');

				var toggleTarget = [];

				if (isToggleParentContainer == 'true') {
					// Target selector only as closest parent.
					// This var has to be an array!
					toggleTarget = [toggleItem.closest(toggleTargetSelector)];
				} else {
					// Using the new toggle target selector, find that within the DOM.
					toggleTarget = document.querySelectorAll(toggleTargetSelector);
				}


				// Get the css class that'll be applied to the element to toggle it.
				var toggleClassName = toggleItem.getAttribute('data-toggle-class');

				// Add an event listener to the button.
				toggleItem.addEventListener('click', function(e) {
					// Cancel the default event.
					e.preventDefault();

					// If this button already has the class, remove it.
					if (this.classList.contains('is-active')) {
						this.classList.remove('is-active');
					} else {
						// When the button is clicked apply a class to it...
						this.className += ' is-active';
					}

					// And apply a class to the target.
					forEach(toggleTarget, function(index, target) {
						// If the target already has the class, remove it.
						if (target.classList.contains(toggleClassName)) {
							target.classList.remove(toggleClassName);
						} else {
							// When the button is clicked apply a class to it...
							target.className += ' ' + toggleClassName;
						}
					});
				});
			});
		}
	})(document, forEach);
})(jQuery);

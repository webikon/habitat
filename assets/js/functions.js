// Functions which are used in theme js

// forEach method
// See https://toddmotto.com/ditch-the-array-foreach-call-nodelist-hack/
// for more information.
var forEach = function(array, callback, scope) {
	for (var i = 0; i < array.length; i++) {
		callback.call(scope, i, array[i]);
		// passes back stuff we need
	}
};

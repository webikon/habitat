( function($) {
	$(document).foundation();

	// Main menu mobile sublist expand
	$(document).on('click', '.js-main-menu-expand-sublist', function() {
		$(this).closest('.js-main-menu-item').toggleClass('is-submenu-expanded')
	});

//  --- Add Active link for Secondary navigation ---
	$("#js-secondary-nav__list a").click(function(e)
	{

		var link = $(this);
		var item = link.parent("li");

		if (item.hasClass("active"))
			{
				item.removeClass("active").children("a").removeClass("active");
			}
			else
			{
				item.addClass("active").children("a").addClass("active");
			}

			if (item.children("ul").length > 0)
			{
					var href = link.attr("href");

					link.attr("href", "#");

					setTimeout(function () { link.attr("href", href); }, 300);

					e.preventDefault();
			}

		}).each(function()
		{

			var link = $(this);

			if (link.get(0).href === location.href)
			{
				link.addClass("active").parents("li").addClass("active");return false;
			}
		});


//  --- Priority menu for Secondary navigation---
// link : https://css-tricks.com/the-priority-navigation-pattern/

	var btn = $('#js-secondary-nav__btn').show();
	var links = $('#js-secondary-nav__list');
	var hiddenLinks = $('#js-secondary-nav__hidden-links');
	var numberItems = 0;
	var totalWidth = 0;
	var breakWidths = [];

	// Initial state width
	links.children().outerWidth(function(i, w) {
		totalWidth += w;
		numberItems += 1;
		breakWidths.push(totalWidth);
	});

	var availableWidth, numberVisibleItems, requiredWidth;

	function control_space() {

		// Get instant state
		availableWidth = links.width() - 10;
		numberVisibleItems = links.children().length;
		requiredWidth = breakWidths[numberVisibleItems - 1];

		// There is not enought space (width)
		if (requiredWidth > availableWidth) {
			links.children().last().prependTo(hiddenLinks);
			numberVisibleItems -= 1;

			control_space();
		}
		// There is more than enough space
		else if (availableWidth > breakWidths[numberVisibleItems]) {
			hiddenLinks.children().first().appendTo(links);
			numberVisibleItems += 1;
		}
		// Update the button accordingly
		if (numberVisibleItems === numberItems) {
			btn.addClass('hidden');
		}
		else {
			btn.removeClass('hidden');
		}
	}

	// Window listeners
	$(window).resize(function() {
		control_space();
	});

	btn.on('click', function() {

		hiddenLinks.toggleClass('hidden');
		btn.toggleClass('is-active');
	});

	control_space();


})(jQuery);

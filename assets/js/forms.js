( function($) {

	var inputs = 'input[type="text"], input[type="email"], input[type="url"], input[type="tel"], textarea, select, input[type="radio"], input[type="checkbox"]';

	/**
	 * Add is-focused class to parent field wrapper so we can style it properly
	 */
	$(inputs).on( 'focus', function() {
		$(this).closest('.gfield').addClass('is-focused');
	}).on( 'blur', function() {
		$(this).closest('.gfield').removeClass('is-focused');
	});

	/**
	 * Disable field if .gfield has class 'is-disabled'
	 */
	$('.gfield.is-disabled').find( inputs ).attr('disabled', "");

})(jQuery);

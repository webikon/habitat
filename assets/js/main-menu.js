/* using forEach function */

(function($) {
	var mainMenu = function(document, forEach) {
		'use strict';

		var focusClass = function focusClass(toggledClass) {
			// Find all menu items.
			var menuItems = document.querySelectorAll('.js-main-menu-item');

			// Create a 'cloak' element that expands over the entire page
			// beneath the menu. If a user clicks on this element, we remove
			// focus from any menu items.
			// This allows the user to click anywhere in the document to remove focus.
			var menuCloak = document.createElement('a');
			menuCloak.href = '#';
			menuCloak.id = 'js-main-menu-cloak';
			menuCloak.className = 'main-menu__cloak';
			menuCloak.onclick = function(e) {
				// Prevent the page moving when the cloak is clicked.
				e.preventDefault();

				// Remove the toggledClass from the cloak.
				this.classList.remove(toggledClass);

				// Remove any instances of the toggledClass within the menu.
				var menuToggled = Array.prototype.slice.call(document.querySelectorAll('#js-main-menu .' + toggledClass));
				menuToggled.map(function(toggled) {
					return toggled.classList.remove(toggledClass);
				});
			};

			// Append the new cloak <a> to the body.
			document.body.appendChild(menuCloak);

			// For each item, add an EventListener.
			// This adds a class name when the link is focused.
			forEach(menuItems, function(index, menuItem) {
				// Get the selector from the selector parent.
				var itemLink = menuItem.querySelectorAll('.js-main-menu-link');

				// Set the initial hasTouched state.
				var hasTouched = false;

				// Add an event listener to the selector.
				itemLink[0].addEventListener('click', function(e) {
					if ('ontouchstart' in document.documentElement) {
						// If this is the first time this link is tapped, don't
						// follow the link. Update the hasTouched variable so
						// the next time the same link is tapped, follow the link.
						if (hasTouched === false) {
							hasTouched = true;
							// If the focus is active, activate the cloak.
							menuCloak.className += ' ' + toggledClass;
							return e.preventDefault();
						}
					}
				});

				// Add an event listener to the selector.
				itemLink[0].addEventListener('focus', function(e) {
					// Add the class to the parent element.
					menuItem.className += ' ' + toggledClass;
					// If the focus is active, activate the cloak.
					menuCloak.className += ' ' + toggledClass;
				});

				// Find the last menu item on each submenu and convert an array.
				var itemLastLinkArray = Array.prototype.slice.call(menuItem.querySelectorAll('.js-main-menu-last'));

				// Get the last menu item within the child list.
				if (itemLastLinkArray.length > 0) {
					var itemLastLink = itemLastLinkArray.slice(-1)[0];

					// On blur remove the class from the parent element.
					itemLastLink.addEventListener('blur', function(e) {
						menuItem.classList.remove(toggledClass);
						// If the blur is active, deactivate the cloak.
						menuCloak.classList.remove(toggledClass);
					});
				}
			});
		};

		return {
			focusClass: focusClass
		};
	}(document, forEach);

	// Pass in the class you'd like applied to shown elements.
	mainMenu.focusClass('is-shown');
})(jQuery);

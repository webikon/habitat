<?php

// Disable default CSS
add_filter( 'pre_option_rg_gforms_disable_css', '__return_true' );
// Turn on HTML5
add_filter( 'pre_option_rg_gforms_enable_html5', '__return_true' );

/**
 * Customize error message markup.
 */
add_filter( 'gform_validation_message', function ( $validation_errors_markup, $form ) {
	$validation_errors_markup = str_replace( "<h2 ", "<p ", $validation_errors_markup );
	$validation_errors_markup = str_replace( "</h2> ", "</p> ", $validation_errors_markup );

	return $validation_errors_markup;
}, 10, 2 );

/**
 * Change section field markup.
 */
add_filter( 'gform_field_content', function ( $field_content, $field, $value, $entry_id, $form_id ) {
	if ( $field->type == 'section' ) {
		$title_class =  ( ! $field->cssClass ) ? 'alt1' : $field->cssClass;

		$field_content = '<div class="section-title section-title--' . $title_class . '">
							<h2 class="section-title__heading">' . esc_html( $field->label ) .'</h2>
						</div>';
	}

	return $field_content;
}, 10, 5 );

/**
 * Add unique css class to .gfield element, so we can use it to style this element separately
 */
add_filter( 'gform_field_css_class', 'custom_class', 10, 3 );
function custom_class( $classes, $field, $form ) {
	if ( $field->type == 'checkbox' ) {
		$classes .= ' gfield_checkbox_wrapper';
	}

	if ( $field->type == 'radio' ) {
		$classes .= ' gfield_radio_wrapper';
	}

	return $classes;
}

/**
 * Add icon wrapper to custom radio field
 */
add_filter( 'gform_field_choice_markup_pre_render', function ( $choice_markup, $choice, $field, $value ) {
	if ( $field->type == 'radio' || $field->type == 'checkbox' ) {
		$new_string = '</label><span class="gform_choice_icon"></span>';

		$choice_markup = str_replace( "</label>", $new_string, $choice_markup );
	}

	return $choice_markup;
}, 10, 4 );


/**
 * Change submit button markup
 */
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
	// For grouped forms, we need different button classes
	if ( isset( $form['cssClass'] ) && strpos( $form['cssClass'], 'gform_group' ) !== false ) {
		$button_classes = 'btn btn--form-cta';
	} else {
		$button_classes = 'btn btn--donate btn--xlarge btn--centered';
	}

	return "<button class='{$button_classes}' id='gform_submit_button_{$form['id']}'>{$form['button']['text']}</button>";
}

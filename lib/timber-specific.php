<?php

if ( file_exists( get_template_directory() . '/vendor/autoload.php' ) ) {
	// Load composer files
	require_once( get_template_directory() . '/vendor/autoload.php' );

} else {
	// If the composer autoload not exists, print a notice in the admin.
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not found. Make sure you ran <strong>composer install</strong> in your theme.</p></div>';
	} );

	return;
}

// Initialize Timber
$timber = new \Timber\Timber();


/**
 * Access data site-wide.
 * This function adds data to the global context of your site.
 * In less-jargon-y terms, any values in this function are available on any view of your website.
 * Anything that occurs on every page should be added here.
 */
function habitat_add_to_context( $context ) {
	// Our menu occurs on every page, so we add it to the global context.
	$context['menu'] = new TimberMenu( 'primary' );
	$context['footer_menu'] = new TimberMenu( 'footer' );

	if ( function_exists( 'pll_the_languages' ) ) {
		$context['lang_switcher'] = pll_the_languages( array(
			'echo' => 0,
			'show_names' => 0,
			'show_flags' => 1,
			'raw' => 1,
			'hide_if_empty' => 0,
		) );
	}

	// Widgets
	$context['widgets'] = array(
		'footer1' => Timber::get_widgets( 'footer-1' ),
		'aside1' => Timber::get_widgets( 'aside-1' ),
	);

	// Prepare all theme icons
	$context['site_logo'] = get_field( 'site_logo_url', 'option' );

	$context['icons'] = habitat_get_all_icons();

	// Footer
	$context['footer'] = array(
		'copyright' => pll__( get_field( 'copyright', 'option' ) ),
		'brands' => get_field( 'brands', 'option' ),
	);

	// Donate button
	$donate_page_id = pll__( get_field( 'donate_page_id', 'option' ) );
	$donate_page_btn_text = pll__( get_field( 'donate_btn_text', 'option' ) );
	$context['donate'] = array(
		'url' => $donate_page_id ? get_permalink( $donate_page_id ) : '',
		'text' => $donate_page_btn_text ? $donate_page_btn_text : __( 'Donate', 'habitat' ),
	);

	// Search label
	$context['search_string'] = get_search_query();

	// Social shortcode for Article Meta
	$context['social_shortcode'] = get_field( 'social_shortcode_monarch', 'option' );

	// Social media urls and "slugs"

	$context['target_blank'] = get_field( 'target_blank', 'option' );

	$context['social_media'] = array();
	if ( $facebook_url = pll__( get_field( 'facebook_url', 'option' ) ) ) {
		$context['social_media'][] = array(
			'url' => $facebook_url,
			'icon' => 'facebook',
		);
	}
	if ( $twitter_url = pll__( get_field( 'twitter_url', 'option' ) ) ) {
		$context['social_media'][] = array(
			'url' => $twitter_url,
			'icon' => 'twitter',
		);
	}
	if ( $youtube_url = pll__( get_field( 'youtube_url', 'option' ) ) ) {
		$context['social_media'][] = array(
			'url' => $youtube_url,
			'icon' => 'youtube',
		);
	}
	if ( $pinterest_url = pll__( get_field( 'pinterest_url', 'option' ) ) ) {
		$context['social_media'][] = array(
			'url' => $pinterest_url,
			'icon' => 'pinterest',
		);
	}
	if ( $instagram_url = pll__( get_field( 'instagram_url', 'option' ) ) ) {
		$context['social_media'][] = array(
			'url' => $instagram_url,
			'icon' => 'instagram',
		);
	}
	if ( $linkedin_url = pll__( get_field( 'linkedin_url', 'option' ) ) ) {
		$context['social_media'][] = array(
			'url' => $linkedin_url,
			'icon' => 'linkedin',
		);
	}

	// This 'site' context below allows you to access main site information like the site title or description.
	$context['site'] = new TimberSite();

	return $context;
}
add_filter( 'timber_context', 'habitat_add_to_context' );

/**
 * Here you can add your own fuctions to Twig. Don't worry about this section if you don't come across a need for it.
 * See more here: http://twig.sensiolabs.org/doc/advanced.html
 *
 * @param object $twig
 */
function habitat_add_to_twig( $twig ) {
	$twig->addExtension( new Twig_Extension_StringLoader() );
	$twig->addFilter( 'myfoo', new Twig_Filter_Function( 'myfoo' ) );

	return $twig;
}
// add_filter( 'get_twig', 'habitat_add_to_twig' );


/**
 * Prepare args for Archive Teaser Grid query, if exists.
 *
 * @param object $post - TimberPost object, from which we get flexible content field
 * @return void
 */
function habitat_archive_teaser_grid_query_args( $post ) {
	if ( ! isset( $post ) ) {
		return;
	}

	$args = '';

	$layout_builder = get_field('layout_builder', $post);
	if ( $layout_builder ) {

		foreach ( $layout_builder  as  $component ) {
			if ( isset( $component['acf_fc_layout'] ) && 'archive-teaser-grid' == $component['acf_fc_layout'] ) {
				// If not set, fallback to all
				$posts_per_page = ! empty( $component['posts_per_page'] ) ? $component['posts_per_page'] : '100';

				global $paged;
				if ( ! isset( $paged ) || ! $paged ) {
					$paged = 1;
				}

				// Create and pass Timber query
				$args = array(
					'post_type' => $component['post_type'],
					'posts_per_page' => $posts_per_page,
					'paged' => $paged,
				);

				if ( ! empty( $component['taxonomy'] ) && ! empty( $component[$component['taxonomy']] ) ) {
					$args['tax_query'] = array(
						array(
							'taxonomy' => $component['taxonomy'],
							'field'    => 'term_id',
							'terms'    => $component[$component['taxonomy']],
						),
					);

				}

			}
		}
	}

	return $args;
}

/**
 * Prepare args for Related posts flexible component.
 *
 * @param object $post - TimberPost object, from which we get flexible content field
 * @return void
 */
function habitat_flexible_related_posts_query_args( $post ) {
	if ( ! isset( $post ) ) {
		return;
	}

	$args = array();

	$layout_builder = get_field('layout_builder', $post);
	if ( $layout_builder ) {

		foreach ( $layout_builder  as  $component ) {

			if ( ( isset( $component['acf_fc_layout'] ) && 'related-posts-grid' == $component['acf_fc_layout'] ) || is_singular( array( 'post', 'event' ) ) ) {
				$post_id = $post->id;

				$args = habitat_related_posts_query_args( $post );
			}
		}
	}

	return $args;
}

/**
 * Prepare args for Related Posts Grid query.
 * Use current taxonomies to get related posts.
 *
 * Source: https://www.cssigniter.com/programmatically-get-related-wordpress-posts-easily/
 *
 * @param object $post - TimberPost object, from which we get flexible content field
 * @return void
 */
function habitat_related_posts_query_args( $post ) {
	if ( ! isset( $post ) ) {
		return;
	}

	$args = '';
	$post_id = $post->id;

	// Get proper taxonomies according to post type
	$post_type = $post->post_type;
	if ( $post_type == 'post' ) {
		$taxonomies = array( 'category', 'tag' );
	} elseif ( $post_type == 'event' ) {
		$taxonomies = array( 'event_type', 'event_country' );
	}

	if ( ! isset( $taxonomies ) ) {
		return;
	}

	$related_args = array(
		'post_type' => $post_type,
		'posts_per_page' => 3,
		'post_status' => 'publish',
		'post__not_in' => array( $post_id ),
		'tax_query' => array(),
	);

	// Add tax_query for each defined taxonomy
	foreach ( $taxonomies as $taxonomy ) {
		$terms = get_the_terms( $post_id, $taxonomy );
		if ( empty( $terms ) || is_wp_error( $terms ) ) {
			continue;
		}

		$term_list = wp_list_pluck( $terms, 'slug' );
		$related_args['tax_query'][] = array(
			'taxonomy' => $taxonomy,
			'field' => 'slug',
			'terms' => $term_list
		);
	}

	// Add proper relation
	if ( count( $related_args['tax_query'] ) > 1 ) {
		$related_args['tax_query']['relation'] = 'OR';
	}

	// Pass proper args, only when there is some tax_query,
	if ( ! empty( $related_args['tax_query'] ) ) {
		$args = $related_args;
	}

	return $args;
}

/**
 * Prepare args for Sub Pages Grid query, if exists.
 *
 * @param object $post - TimberPost object, from which we get flexible content field
 * @return void
 */
function habitat_sub_pages_grid_query_args( $post ) {
	if ( ! isset( $post ) ) {
		return;
	}

	$layout_builder = get_field('layout_builder', $post);
	if ( $layout_builder ) {

		foreach ( $layout_builder  as  $component ) {

			if ( isset( $component['acf_fc_layout'] ) && 'sub-pages-grid' == $component['acf_fc_layout'] ) {
				// Create and pass Timber query
				$args = array(
					'post_type' => 'page',
					'posts_per_page' => '100',
					'post_parent' => $post->ID,
					'order' => 'ASC',
					'orderby' => 'menu_order'
				);

				return $args;
			}
		}
	}

}

/**
 * Prepare args for custom archive.
 * Used on templates for Events, Partners, Team Members, ...
 *
 * @param string $post_type
 * @return void
 */
function habitat_archive_query_args( $post_type ) {
	if ( ! $post_type ) {
		return;
	}

	global $paged;
	if ( ! isset( $paged ) || ! $paged ) {
		$paged = 1;
	}

	$args = array(
		'post_type' => $post_type,
		'paged' => $paged,
	);

	// Set custom number or fallback to WP default (in Reading settings)
	$posts_per_page = get_field( 'archive_posts_per_page' );
	if ( $posts_per_page  ) {
		$args['posts_per_page'] = $posts_per_page;
	}

	return $args;
}

 /* Prepare args for Secondary Navigation
 *
 * @param object $post - TimberPost object
 * @return void
 */
function habitat_secondary_navigation_query_args( $post ) {

	if ( ! isset( $post ) ) {
		return;
	}

	$page_id = ( '0' != $post->post_parent ? $post->post_parent : $post->ID );

	$args = array(
		'post_type' => 'page',
		'posts_per_page' => '-1',
		'post_parent' => $page_id,
		'order' => 'ASC',
		'orderby' => 'menu_order'
	);

	return $args;
}

/**
 * Get proper image size based on current grid layout or modifier class.
 *
 * @param string $grid_layout
 * @param string $modifier_class
 * @return array
 */
function habitat_teaser_image_size( $grid_layout, $modifier_class ) {
	$img_size = array();

	switch ( $grid_layout ) {
		case 'l-two-up' :
			$img_size = array( 800, 450 );
			break;
		case 'l-three-up' :
			$img_size = array( 390, 220 );
			break;
		case 'l-four-up' :
			$img_size = array( 340, 170 );
			break;

		default:
			$img_size = array( 390, 220 );
			break;
	}

	// fullwidth teaser has bigger size
	if ( $modifier_class == 'teaser--sbs' ) {
		$img_size = array( 800, 450 );
	}

	return $img_size;
}

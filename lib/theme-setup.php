<?php
/**
 * This function applies some fundamental WordPress setup, as well as our functions to include custom post types and taxonomies.
 */
function habitat_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on _s, use a find and replace
	 * to change '_s' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'habitat', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Editor style for nicer typography in TinyMCE editor.
	if ( WP_DEBUG ) {
		add_editor_style( get_template_directory_uri() . '/dist/css/editor-style.css' );
	} else {
		add_editor_style( get_template_directory_uri() . '/dist/css/editor-style.min.css' );
	}

	// Reduce image quality a link_title
	add_filter( 'jpeg_quality', function() { return 85; } );

}
add_action( 'after_setup_theme', 'habitat_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function habitat_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'habitat_content_width', 640 );
}
add_action( 'after_setup_theme', 'habitat_content_width', 0 );

if ( function_exists('acf_add_options_page') ) {

	acf_add_options_page( array(
		'page_title' 	=> __( 'Theme General Settings', 'habitat' ),
		'menu_title'	=> __( 'Theme Settings', 'habitat' ),
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	) );

}


function habitat_missing_mu_plugins_error() {
	$class = 'notice notice-error';
	$message_cpt = __( 'CPT_Core mu-plugin is missing. Custom post types won\'t be displayed', 'habitat' );
	$message_tax = __( 'Taxonomy_Core mu-plugin is missing. Custom taxonomies won\'t be displayed', 'habitat' );

	if ( ! file_exists( WPMU_PLUGIN_DIR . '/CPT_Core/CPT_Core.php' ) ) {
		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message_cpt ) );
	}

	if ( ! file_exists( WPMU_PLUGIN_DIR . '/Taxonomy_Core/Taxonomy_Core.php' ) ) {
		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message_tax ) );
	}
}
add_action( 'admin_notices', 'habitat_missing_mu_plugins_error' );

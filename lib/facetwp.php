<?php

/**
 * Custom FacetWP template HTML.
 *
 * Source: https://facetwp.com/documentation/facetwp_template_html/
 */
add_filter( 'facetwp_template_html', function( $output, $class ) {
	$GLOBALS['wp_query'] = $class->query;
	$context['posts'] = Timber::get_posts();
	ob_start();

	// render table rows
	Timber::render( 'partials/table-display-body.twig', $context );

	return ob_get_clean();
}, 10, 2 );

/**
 * This filter lets you override the Query Arguments field from FacetWP templates.
 * The Query arguments array (used by WP_Query) tells WP which posts to retrieve from the database.
 *
 * Source: https://facetwp.com/documentation/facetwp_query_args/
 */
add_filter( 'facetwp_query_args', function( $query_args, $class ) {
	$query_args = array(
		'post_type' => 'event',
		'posts_per_page' => 10,
	);

	return $query_args;
}, 10, 2 );

/**
 * Remove FacetWP frontend css
 *
 * Source: https://facetwp.com/documentation/facetwp_assets/
 */
add_filter( 'facetwp_assets', function( $assets ) {
	unset( $assets['front.css'] );
	return $assets;
});

/**
 * Autodetect custom WP_Query, if it has 'facetwp' argument.
 *
 * Source: https://facetwp.com/documentation/facetwp_is_main_query/
 */
add_filter( 'facetwp_is_main_query', function( $is_main_query, $query ) {
	if ( isset( $query->query_vars['facetwp'] ) ) {
		$is_main_query = (bool) $query->query_vars['facetwp'];
	}
	return $is_main_query;
}, 10, 2 );

/**
 * Register new facet for event filtering.
 *
 * Source: https://facetwp.com/documentation/facetwp_facets/
 */
add_filter( 'facetwp_facets', function( $facets ) {
	$facets[] = array(
		'label' => 'Events type filter',
		'name' => 'event_type_filter',
		'type' => 'radio',
		'search_engine' => '',
		'source' => 'tax/event_type',
		'orderby' => 'count',
		'ghosts' => 'no',
		'count' => 100,
	);

	$facets[] = array(
		'label' => 'Events country filter',
		'name' => 'event_country_filter',
		'type' => 'radio',
		'search_engine' => '',
		'source' => 'tax/event_country',
		'orderby' => 'count',
		'ghosts' => 'no',
		'count' => 100,
	);

	return $facets;
});


/**
 * Allow this because Load more pager.
 *
 * Source: https://facetwp.com/how-to-add-a-load-more-button/
 */
add_filter( 'facetwp_template_force_load', '__return_true' );


/**
 * Change FWP radio inputs output.
 * Add reset input and adjust radio element body.
 *
 */
add_filter( 'facetwp_facet_html', function( $output, $params ) {
	if ( 'event_type_filter' == $params['facet']['name'] || 'event_country_filter' == $params['facet']['name'] ) {
		$facet = $params['facet'];

		$output = '';
		$values = (array) $params['values'];
		$selected_values = (array) $params['selected_values'];

		$key = 0;

		// Create additional input, which can reset current FWP filter
		$selected = ! $selected_values ? ' checked' : '';
		$output .= '<div class="facetwp-radio-reset js-fwp-reset ' . $selected . '">' ;
		$output .= '<div class="facetwp-radio-input">';
		$output .= '<span class="facetwp-radio-icon"></span>';
		$output .= __( 'All', 'habitat' );
		$output .= '</div>';
		$output .= '</div>';

		foreach ( $values as $key => $result ) {
			$selected = in_array( $result['facet_value'], $selected_values ) ? ' checked' : '';
			$selected .= ( 0 == $result['counter'] && '' == $selected ) ? ' disabled' : '';
			$output .= '<div class="facetwp-radio' . $selected . '" data-value="' . esc_attr( $result['facet_value'] ) . '">';
			$output .= '<div class="facetwp-radio-input">';
			$output .= '<span class="facetwp-radio-icon"></span>';
			$output .= esc_html( $result['facet_display_value'] ) . ' <span class="facetwp-counter">(' . $result['counter'] . ')</span>';
			$output .= '</div>';
			$output .= '</div>';
		}
	}
	return $output;
}, 10, 2 );

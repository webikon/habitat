<?php

//  Custom setting for Breadcrumb plugin : https://wordpress.org/plugins/breadcrumb-navxt/
add_filter('bcn_breadcrumb_title', function( $title, $type, $id ) {
	if ( $type[0] === 'home' ) {
		$title = __( 'Home', 'habitat' );
	}

	return $title;
}, 42, 3);

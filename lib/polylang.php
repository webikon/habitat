<?php
/**
 * Register ACF Options as Polylang strings, so they can be translated
 */
if ( function_exists( 'get_field' ) && function_exists( 'pll_register_string' ) ) {
	$facebook = get_field( 'facebook_url', 'option' );
	$twitter = get_field( 'twitter_url', 'option' );
	$youtube = get_field( 'youtube_url', 'option' );
	$pinterest = get_field( 'pinterest_url', 'option' );
	$instagram = get_field( 'instagram_url', 'option' );
	$linkedin = get_field( 'linkedin_url', 'option' );
	if ( $facebook ) {
		pll_register_string( 'facebook_url', $facebook, 'habitat-custom' );
	}

	if ( $twitter ) {
		pll_register_string( 'twitter_url', $twitter, 'habitat-custom' );
	}

	if ( $youtube ) {
		pll_register_string( 'youtube_url', $youtube, 'habitat-custom' );
	}

	if ( $pinterest ) {
		pll_register_string( 'pinterest_url', $pinterest, 'habitat-custom' );
	}

	if ( $instagram ) {
		pll_register_string( 'instagram_url', $instagram, 'habitat-custom' );
	}

	if ( $linkedin ) {
		pll_register_string( 'linkedin_url', $linkedin, 'habitat-custom' );
	}

	$copyright = get_field( 'copyright', 'option' );
	if ( $copyright ) {
		pll_register_string( 'copyright', $copyright, 'habitat-custom', true );
	}

	$donate_page_id = get_field( 'donate_page_id', 'option' );
	if ( $donate_page_id ) {
		pll_register_string( 'donate_page_id', $donate_page_id, 'habitat-custom' );
	}

	$donate_btn_text = get_field( 'donate_btn_text', 'option' );
	if ( $donate_btn_text ) {
		pll_register_string( 'donate_btn_text', $donate_btn_text, 'habitat-custom' );
	}

}

// Fallback
if ( ! function_exists( 'pll__' ) ) {
	function pll__( $string ) {
		return $string;
	}
}

<?php

/**
 * Redirect event taxonomy archives to Events page and set proper FWP filter.
 *
 * @return void
 */
function habitat_redirect_event_tax_archives() {
	$events_page_id = get_field( 'events_page_id', 'option' );
	if ( ! $events_page_id ) {
		return;
	}

	$tax_array = array( 'event_type', 'event_country' );
	if ( is_tax( $tax_array ) ) {
		$queried_object = get_queried_object();
		$current_tax = $queried_object->taxonomy;
		$term_slug = $queried_object->slug;

		if( $current_tax && $term_slug ) {
			wp_redirect( esc_url( add_query_arg( 'fwp_' . $current_tax. '_filter', $term_slug,  get_permalink( $events_page_id ) ) ) );
			exit();
		}
	}
}
add_action( 'template_redirect', 'habitat_redirect_event_tax_archives' );

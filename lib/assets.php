<?php
/**
 * Theme assets.
 *
 * @package  WordPress
 */

/**
 * Enqueue scripts and styles.
 */
function habitat_assets() {
	// Enqueue our stylesheet and JS file with a jQuery dependency.
	// Load different assets depending on the environment (WP_DEBUG on LIVE env should be disabled).
	if ( WP_DEBUG ) {
		$min = '';
	} else {
		$min = '.min';
	}

	// Enqueue external assets here
	wp_enqueue_script( 'habitat-picturefill', get_template_directory_uri() . '/dist/js/vendor/picturefill' . $min . '.js', array( 'jquery' ), '0.0.1', true );

	wp_enqueue_style( 'habitat-featherlight-style', get_template_directory_uri() . '/dist/css/vendor/featherlight' . $min . '.css', array(), '0.0.1' );

	wp_enqueue_script( 'habitat-featherlight-js', get_template_directory_uri() . '/dist/js/vendor/featherlight' . $min . '.js', array( 'jquery' ), '0.0.1', true );


	// These assets should be loaded last, so enqueue external assets above this line
	wp_enqueue_style( 'habitat-style', get_template_directory_uri() . '/dist/css/style' . $min . '.css', array(), '1.2.0' );
	wp_enqueue_script( 'habitat-theme', get_template_directory_uri() . '/dist/js/theme' . $min . '.js', array( 'jquery' ), '1.2.0', true );

	if ( is_page_template( 'template-events.php' ) ) {
		wp_enqueue_script( 'habitat-fwp-custom', get_template_directory_uri() . '/dist/js/facetwp-custom' . $min . '.js', array( 'jquery' ), '0.0.1', true );
	}

}
add_action( 'wp_enqueue_scripts', 'habitat_assets' );

/**
 * Asynchronously load defined scripts
 *
 * Source: https://matthewhorne.me/defer-async-wordpress-scripts/
 */
function habitat_async_script_atts( $tag, $handle ) {
	// add script handles to the array below
	$scripts_to_async = array( 'habitat-picturefill' );

	foreach ( $scripts_to_async as $async_script ) {
		if ( $async_script == $handle ) {
			return str_replace( ' src', ' async="async" src', $tag );
		}
	}

	return $tag;
}
add_filter( 'script_loader_tag', 'habitat_async_script_atts', 10, 2 );

<?php
/**
 * Custom Menus.
 * Note that you only need the arguments and register_post_type function here. They are hooked into WordPress in functions.php.
 *
 * @package  WordPress
 */

/**
* Load CPT_Core.
*
* @link https://github.com/WebDevStudios/CPT_Core
*/
if ( ! file_exists( WPMU_PLUGIN_DIR . '/CPT_Core/CPT_Core.php' ) ) {
	return;
}

require_once WPMU_PLUGIN_DIR . '/CPT_Core/CPT_Core.php';

class Team_member_CPT extends CPT_Core {

	/**
	 * Register Custom Post Types. See documentation in CPT_Core, and in wp-includes/post.php
	 */
	public function __construct() {

		// Register this cpt
		// First parameter should be an array with Singular, Plural, and Registered name
		parent::__construct(
			array(
				__( 'Team member', 'habitat-cpt' ),
				__( 'Team members', 'habitat-cpt' ),
				'team_member',
			),
			array(
				'supports' => array( 'title', 'excerpt', 'thumbnail' ),
				'has_archive'         => false,
				'publicly_queryable'  => false,
				'exclude_from_search' => false,
				'menu_icon'           => 'dashicons-groups',
				'rewrite'			=> array(
					'slug' 				=> _x( 'team-member', 'Team members slug', 'habitat-cpt' ),
					'with_front' 	=> false
				),
			)
		);

	}

}
new Team_member_CPT();

class Partners_CPT extends CPT_Core {

	/**
	 * Register Custom Post Types. See documentation in CPT_Core, and in wp-includes/post.php
	 */
	public function __construct() {

		// Register this cpt
		// First parameter should be an array with Singular, Plural, and Registered name
		parent::__construct(
			array(
				__( 'Partner', 'habitat-cpt' ),
				__( 'Partners', 'habitat-cpt' ),
				'partner',
			),
			array(
				'supports' => array( 'title', 'excerpt', 'thumbnail' ),
				'has_archive'         => false,
				'publicly_queryable'  => false,
				'exclude_from_search' => true,
				'menu_icon'           => 'dashicons-businessman',
				'rewrite'			=> array(
					'slug' 				=> _x( 'partner', 'Partners slug', 'habitat-cpt' ),
					'with_front' 	=> false
				),
			)
		);

	}

}
new Partners_CPT();

class Press_release_CPT extends CPT_Core {

	/**
	 * Register Custom Post Types. See documentation in CPT_Core, and in wp-includes/post.php
	 */
	public function __construct() {

		// Register this cpt
		// First parameter should be an array with Singular, Plural, and Registered name
		parent::__construct(
			array(
				__( 'Press release', 'habitat-cpt' ),
				__( 'Press releases', 'habitat-cpt' ),
				'press_release',
			),
			array(
				'supports' => array( 'title', 'excerpt', 'thumbnail' ),
				'has_archive'         => false,
				'publicly_queryable'  => false,
				'exclude_from_search' => false,
				'menu_icon'           => 'dashicons-clipboard',
				'rewrite'			=> array(
					'slug' 				=> _x( 'press-release', 'Press release slug', 'habitat-cpt' ),
					'with_front' 	=> false
				),
			)
		);

	}

}
new Press_release_CPT();

class Event_CPT extends CPT_Core {

	/**
	 * Register Custom Post Types. See documentation in CPT_Core, and in wp-includes/post.php
	 */
	public function __construct() {

		// Register this cpt
		// First parameter should be an array with Singular, Plural, and Registered name
		parent::__construct(
			array(
				__( 'Event', 'habitat-cpt' ),
				__( 'Events', 'habitat-cpt' ),
				'event',
			),
			array(
				'supports' => array( 'title', 'editor', 'thumbnail' ),
				'has_archive'         => false,
				'menu_icon'           => 'dashicons-tickets-alt',
				'rewrite'			=> array(
					'slug' 				=> _x( 'event', 'Event slug', 'habitat-cpt' ),
					'with_front' 	=> false,
				),
			)
		);

	}

}
new Event_CPT();

<?php
/**
 * Image enhancements and customizations
 */


/**
 * Adjust thumnail sizes, remove unused ones.
 */
function habitat_adjust_image_sizes() {
	// Remove sizes, so they won't generate
	add_image_size( 'medium_large', 0, 0 );

	// Adjust Large size (article width)
	add_image_size( 'large', 760, 0 );
}
add_action( 'init', 'habitat_adjust_image_sizes' );

/**
 * Add rel="lightbox" to content images links
 * Works nicely with http://lokeshdhakar.com/projects/lightbox2/
 *
 * Source: http://www.wprecipes.com/how-to-automatically-add-rellightbox-to-all-images-embedded-in-a-post/
 */
function habitat_content_images_lightbox_rel( $content ) {
	global $post;

	$pattern = "/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
	$replacement = '<a$1href=$2$3.$4$5 rel="lightbox" title="' . $post->post_title . '"$6>';
	$content = preg_replace( $pattern, $replacement, $content );

	return $content;
}
add_filter( 'the_content', 'habitat_content_images_lightbox_rel', 99 );

/**
 * Wrap images with figure tag. Courtesy of Interconnectit http://interconnectit.com/2175/how-to-remove-p-tags-from-images-in-wordpress/
 */
function habitat_img_unautop( $pee ) {
	$pee = preg_replace( '/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<figure>$1</figure>', $pee );

	return $pee;
}
add_filter( 'the_content', 'habitat_img_unautop', 30 );

/**
 * Customized the output of caption, you can remove the filter to restore back to the WP default output.
 * Courtesy of DevPress. http://devpress.com/blog/captions-in-wordpress/
 */
function habitat_cleaner_caption( $output, $attr, $content ) {
	/* We're not worried abut captions in feeds, so just return the output here. */
	if ( is_feed() ) {
		return $output;
	}

	/* Set up the default arguments. */
	$defaults = array (
		'id'      => '',
		'align'   => 'alignnone',
		'width'   => '',
		'caption' => ''
	);
	/* Merge the defaults with user input. */
	$attr = shortcode_atts( $defaults, $attr );
	/* If the width is less than 1 or there is no caption, return the content wrapped between the [caption]< tags. */
	if ( 1 > $attr['width'] || empty( $attr['caption'] ) ) {
		return $content;
	}
	/* Set up the attributes for the caption <div>. */
	$attributes = ' class="media ' . esc_attr( $attr['align'] ) . '"';
	/* Open the caption <div>. */
	$output = '<figure' . $attributes . '>';
	/* Allow shortcodes for the content the caption was created for. */
	$output .= do_shortcode( $content );
	/* Append the caption text. */
	$output .= '<figcaption class="media__caption">' . $attr['caption'] . '</figcaption>';
	/* Close the caption </div>. */
	$output .= '</figure>';

	/* Return the formatted, clean caption. */
	return $output;
}
add_filter( 'img_caption_shortcode', 'habitat_cleaner_caption', 10, 3 );

/**
 * Remove width and height in editor, for a better responsive world.
 */
function habitat_image_editor( $html, $id, $alt, $title ) {
	return preg_replace( array (
			'/\s+width="\d+"/i',
			'/\s+height="\d+"/i',
			'/alt=""/i'
		), array (
			'',
			'',
			'',
			'alt="' . $title . '"'
		), $html );
}
add_filter( 'get_image_tag', 'habitat_image_editor', 0, 4 );

/**
 * Allow SVG upload.
 *
 * @param [type] $mimes
 * @return void
 */
function cc_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';

	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

<?php
/**
 * Custom Menus.
 * Same as with Custom Types, you only need the arguments and register_taxonomy function here. They are hooked into WordPress in functions.php.
 *
 * @package  WordPress
 */

/**
* Load Taxonomy_Core
*
* @link https://github.com/WebDevStudios/Taxonomy_Core
*/
if ( ! file_exists( WPMU_PLUGIN_DIR . '/Taxonomy_Core/Taxonomy_Core.php' ) ) {
	return;
}

require_once WPMU_PLUGIN_DIR . '/Taxonomy_Core/Taxonomy_Core.php';

$event_cat_country = register_via_taxonomy_core(
	array(
		__( 'Country', 'habitat-tax' ), // Singular
		__( 'Countries', 'habitat-tax' ), // Plural
		'event_country', // Registered name
	),
	array(
		'rewrite'			=> array(
			'slug' 				=> _x( 'event-country', 'Event Country taxonomy slug', 'habitat-tax' ),
			'with_front' 	=> false,
		),
	),
	array( 'event' ) // Registered to
);

$event_cat_typ = register_via_taxonomy_core(
	array(
		__( 'Event Type', 'habitat-tax' ), // Singular
		__( 'Event Types', 'habitat-tax' ), // Plural
		'event_type', // Registered name
	),
	array(
		'rewrite'			=> array(
			'slug' 				=> _x( 'event-type', 'Event Type taxonomy slug', 'habitat-tax' ),
			'with_front' 	=> false,
		),
	),
	array( 'event' ) // Registered to
);

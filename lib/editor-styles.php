<?php
/**
 * Callback function to insert 'styleselect' into the $buttons array
 *
 * @param array $buttons
 * @return void
 */
function habitat_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );

	return $buttons;
}
add_filter( 'mce_buttons_2', 'habitat_mce_buttons_2' );

/*
 * Modify TinyMCE editor to remove H1.
 */
function habitat_mce_remove_unused_formats( $init ) {
	// Add block format elements you want to show in dropdown
	$init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Address=address;Pre=pre';

	return $init;
}
add_filter( 'tiny_mce_before_init', 'habitat_mce_remove_unused_formats' );

/**
 * Callback function to filter the MCE settings
 * Add some editor styles formats
 *
 * @param array $init_array
 * @return void
 */
function habitat_mce_before_init_insert_formats( $init_array ) {
	// Define the style_formats array
	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' => __( 'Basic text H2', 'habitat' ),
			'selector' => 'h1,h2,h3,h4,h5,h6',
			'classes' => 'basic-text-h2',
		),
		array(
			'title' => __( 'Basic text H3', 'habitat' ),
			'selector' => 'h1,h2,h3,h4,h5,h6',
			'classes' => 'basic-text-h3',
		),
		array(
			'title' => __( 'Basic text H4', 'habitat' ),
			'selector' => 'h1,h2,h3,h4,h5,h6',
			'classes' => 'basic-text-h4',
		),
		array(
			'title' => __( 'Basic text H5', 'habitat' ),
			'selector' => 'h1,h2,h3,h4,h5,h6',
			'classes' => 'basic-text-h5',
		),
		array(
			'title' => __( 'Basic text H6', 'habitat' ),
			'selector' => 'h1,h2,h3,h4,h5,h6',
			'classes' => 'basic-text-h6',
		),
		array(
			'title' => __( '1/2 paragraph margin', 'habitat' ),
			'selector' => 'p',
			'classes' => 'basic-text-margin-sm',
		),
		array(
			'title' => __( 'Cite', 'habitat' ),
			'inline' => 'cite',
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
add_filter( 'tiny_mce_before_init', 'habitat_mce_before_init_insert_formats' );

<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package  WordPress
 */

// Fallback for ACF get_field() to prevent fatal
add_action( 'init', function() {
	if ( !is_admin() && ! function_exists( 'get_field' ) ) {
		function get_field() {
			return '';
		}
	}
} );

/**
 * Redirect missing and unnecessary pages.
 */
function habitat_redirect_missing_archives() {
	global $wp_query, $post;

	if ( is_attachment() ) {
		$post_parent = $post->post_parent;

		if ( $post_parent ) {
			wp_redirect( get_permalink( $post->post_parent ), 301 );
			exit;
		}

		$wp_query->set_404();

		return;
	}

	if ( is_author() || is_date() ) {
		$wp_query->set_404();
	}
}
add_action( 'template_redirect', 'habitat_redirect_missing_archives' );

/**
 * Hide editor on specific pages.
 *
 */
function habitat_hide_editor() {
	// Get the Post ID.
	if ( isset( $_GET['post'] ) ) {
		$post_id = $_GET['post'];
	} elseif ( isset( $_POST['post'] ) ) {
		$post_id = $_POST['post'];
	}

	if( ! isset( $post_id ) ) {
		return;
	}

	// Hide the editor on front page
	if ( $post_id == get_option( 'page_on_front' ) ){
		remove_post_type_support( 'page', 'editor' );
	}

}
add_action( 'admin_init', 'habitat_hide_editor' );

/**
 * Remove default monarch inline buttons display after/before content.
 * Instead, display those buttons in article meta sidebar.
 */
add_action( 'init', function() {
	if ( ! isset( $GLOBALS['et_monarch'] ) ) {
		return;
	}

	$monarch = $GLOBALS['et_monarch'];

	remove_filter( 'the_content', array( $monarch, 'display_inline' ) );
	add_filter( 'social_share_content', array( $monarch, 'display_inline' ) );
} );

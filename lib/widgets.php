<?php
/**
 * Widgets and Sidebars
 * Register your widgets and sidebars here.
 *
 * @package  WordPress
 */

 register_sidebar( array(
 	'name' => __( 'Footer 1', 'habitat' ),
 	'id' => 'footer-1',
 	'description' => __( 'Widgets in this area will be shown in footer area.', 'habitat' ),
 	'before_widget' => '<div id="%1$s" class="widget %2$s">',
 	'after_widget'  => '</div>',
 	'before_title'  => '<h3 class="widget-title">',
 	'after_title'   => '</h3>',
 ) );

 register_sidebar( array(
 	'name' => __( 'Aside 1', 'habitat' ),
 	'id' => 'aside-1',
 	'description' => __( 'Widgets in this area will be shown in Donate page.', 'habitat' ),
 	'before_widget' => '<div id="" class="aside__section">',
 	'after_widget'  => '</div>',
 	'before_title'  => '<h2 class="aside__heading">',
 	'after_title'   => '</h2>',
 ) );

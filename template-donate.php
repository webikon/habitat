<?php
/**
 * The template for displaying donate form and sidebar.
 *
 * Template Name: Donate
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

// Prepare data for Archive Teaser Grid flexible component (if exists)
$args = habitat_archive_teaser_grid_query_args( $post );
if ( $args ) {
	$context['archive_teaser_posts'] = Timber::get_posts( $args );
}

// Prepare data for Sub Pages Grig flexible component (if exists)
$args = habitat_sub_pages_grid_query_args( $post );
$context['sub_pages'] = Timber::get_posts( $args );

$templates = array( 'template-donate.twig', 'page.twig' );
Timber::render( $templates, $context );

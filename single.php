<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
$context['comment_form'] = TimberHelper::get_comment_form();

// Prepare data for static component on single post/event
$args = habitat_related_posts_query_args( $post );
if ( $args ) {
	$context['related_posts'] = Timber::get_posts( $args );
}

// Prepare data for Related Posts Grid flexible component(if exists)
$flexible_args = habitat_flexible_related_posts_query_args( $post );
if ( $flexible_args ) {
	$context['related_posts'] = Timber::get_posts( $args );
}

// Prepare data for Archive Teaser Grid flexible component (if exists)
$args = habitat_archive_teaser_grid_query_args( $post );
if ( $args ) {
	$context['archive_teaser_posts'] = Timber::get_posts( $args );
}

Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );

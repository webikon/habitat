<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$templates = array( 'page-' . $post->post_name . '.twig', 'page.twig' );

// Prepare data for Secondari Navigation
$args = habitat_secondary_navigation_query_args($post);

if ( $args ) {
	$context['secondary_nav'] = Timber::get_posts( $args );
}

// Prepare data for Archive Teaser Grid flexible component (if exists)
$args = habitat_archive_teaser_grid_query_args( $post );
if ( $args ) {
	$context['archive_teaser_posts'] = new Timber\PostQuery( $args );
}

// Prepare data for Sub Pages Grig flexible component (if exists)
$args = habitat_sub_pages_grid_query_args( $post );
$context['sub_pages'] = Timber::get_posts( $args );

// On frontpage, render other twig template
// (Note: this condition should be in index.php, but have to be here in page.php because of Timber bug)
if ( is_front_page() ) {
	// Set a home page variable
	$context['is_front_page'] = 'true';

	array_unshift( $templates, 'front-page.twig' );
}

Timber::render( $templates, $context );

<?php
/**
 * The template for displaying Press releases archive.
 *
 * Template Name: Pressroom
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

// Custom archive query
$archive_args = habitat_archive_query_args( 'press_release' );
$context['posts'] = new Timber\PostQuery( $archive_args );

// Prepare data for Archive Teaser Grid flexible component (if exists)
$args = habitat_archive_teaser_grid_query_args( $post );
if ( $args ) {
	$context['archive_teaser_posts'] = Timber::get_posts( $args );
}

// Prepare data for Sub Pages Grig flexible component (if exists)
$args = habitat_sub_pages_grid_query_args( $post );
$context['sub_pages'] = Timber::get_posts( $args );

$templates = array( 'template-press-releases.twig', 'archive.twig' );
Timber::render( $templates, $context );
